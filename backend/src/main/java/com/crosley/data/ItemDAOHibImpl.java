package com.crosley.data;

import com.crosley.beans.Items;
import com.crosley.util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

public class ItemDAOHibImpl implements ItemDAO{

    @Override
    public List<Items> getAllItems() {
       try(Session s = HibernateUtil.getSession();) {
           //using HQL to query database...from Items same as "select * from items"
           List<Items> items = s.createQuery("from Items", Items.class).list();
           return items;
       }
    }

    @Override
    public Items getItemById(int id) {
        try(Session s = HibernateUtil.getSession();) {
            Items items = s.createQuery("from Items where itemId = " + id, Items.class).getSingleResult();
            return items;
        }
    }

    @Override
    public Items addNewItem(Items item) {
        try(Session s = HibernateUtil.getSession();) {
            //using HQL to query database...from Items same as "select * from items"
            s.beginTransaction();
            Items items = new Items();
            items.setItemName(item.getItemName());
            items.setItemCategory(item.getItemCategory());
            items.setItemCondition(item.getItemCondition());
            items.setItemQuantity(item.getItemQuantity());
            items.setItemDescription(item.getItemDescription());
            items.setItemPrice(item.getItemPrice());
            items.setPicture(item.getPicture());
            s.save(items);
            s.getTransaction().commit();

            return items;
        }
    }

    @Override
    public void removeItem(int id) {
        try(Session s = HibernateUtil.getSession()) {
            s.beginTransaction();
            Items item = getItemById(id);
            s.delete(item);
            s.getTransaction().commit();
        }
    }
    @Override
    public Items updateItem(Items item) {
        try(Session s = HibernateUtil.getSession()) {
            s.beginTransaction();
            s.update(item);
            s.getTransaction().commit();
        }
        return item;
    }
}
