package com.crosley.services;

import com.crosley.beans.Items;
import com.crosley.data.ItemDAOHibImpl;
import java.util.List;

public class ItemService {
    private final ItemDAOHibImpl itemDaoHibImpl = new ItemDAOHibImpl();

    public List<Items> getAll(){
        return itemDaoHibImpl.getAllItems();
    }
    public Items getItem(int id) {return  itemDaoHibImpl.getItemById(id);}
    public Items addItem(Items items) { return itemDaoHibImpl.addNewItem(items);}
    public void removeItem(int id) {itemDaoHibImpl.removeItem(id); }
    public Items updateItem(Items itemId) { return itemDaoHibImpl.updateItem(itemId);}
}
