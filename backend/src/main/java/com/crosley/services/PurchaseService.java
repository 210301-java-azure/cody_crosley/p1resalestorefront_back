package com.crosley.services;

import com.crosley.beans.Items;
import com.crosley.beans.PurchasedItems;
import com.crosley.controllers.PurchasesController;
import com.crosley.data.PurchasedItemDAOHibImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class PurchaseService {

    PurchasedItemDAOHibImpl PurchasedItemDAOHibImpl = new PurchasedItemDAOHibImpl();
    public List<PurchasedItems> getAll(){ return PurchasedItemDAOHibImpl.getAllPurchases(); }
    public PurchasedItems getPurchase(int id){ return PurchasedItemDAOHibImpl.getPurchasedItemById(id); }
    public void removePurchase(int id){PurchasedItemDAOHibImpl.removePurchasedItem(id);}
    public void createPurchase(int[] purchaseInfo){PurchasedItemDAOHibImpl.addPurchasedItem(purchaseInfo);}
    //public PurchasedItems updatePurchase(PurchasedItems purchasedItems){ return purchasedItemDAO.addPurchasedItem();}
}
