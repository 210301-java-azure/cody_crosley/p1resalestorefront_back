package com.crosley.controllers;

import com.crosley.beans.Items;
import com.crosley.beans.Users;
import com.crosley.customexceptions.wrongInput;
import com.crosley.data.UserDAO;
import com.crosley.services.UserService;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserController {
    private Logger log = LoggerFactory.getLogger(UserController.class);
    UserService userService = new UserService();
//getting all users
    public void handleGetAllUsers(Context ctx) {
        ctx.json(userService.getUsers());
    }

//getting user by id
    public void handleGetUserById(Context ctx) throws wrongInput {
        String userIdString = ctx.pathParam("userId");
        if(userIdString.matches("^\\d+$")){
            int userId = Integer.parseInt(userIdString);
            Users getUser = userService.getUser(userId);
            if(getUser == null){
                throw new NotFoundResponse("User not found");
            }else{
                ctx.json(getUser);
            }
        }
            throw new wrongInput("Must enter a numeric value");
    }

//creating a users
    public void handlePostNewUser(Context ctx){
        Users newUser = ctx.bodyAsClass(Users.class);
        userService.addUser(newUser);
    }

//update a users information
    public void handleUpdateUser(Context ctx){
        Users updateUser = ctx.bodyAsClass(Users.class);
        userService.updateUser(updateUser);
    }

//remove a users by id
    public void handleRemoveUser(Context ctx){
        String idString = ctx.body();
        log.info(idString);
        int idInt = Integer.parseInt(idString);
        userService.removeUser(idInt);

    }

    public void handleGetUserByUsername(Context ctx)  {
        String username = ctx.pathParam("username");
            Users getUser = userService.getByUserName(username);
            log.warn("getting user by name "+ username);
            if(getUser == null){
                throw new NotFoundResponse("User not found");
            }else{
                ctx.json(getUser);
            }

    }
}


