package com.crosley.controllers;

import com.crosley.beans.Users;
import com.crosley.services.UserService;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;


public class AuthController {

    private Logger log = LoggerFactory.getLogger(AuthController.class);
    UserService userService = new UserService();

    public void employeeAuthLogin(Context ctx) {
        final int employeeRole = 1;
        final int customerRole = 2;
        String user = ctx.formParam("username");
        String pass = ctx.formParam("password");
        log.info("attempting to login user " + user);
        Users userLogin = new Users();
        userLogin = userService.getByUserName(user);
        log.info(userLogin.toString());
        if (user != null && user.equals(userLogin.getUsername())) {
            if (pass != null && pass.equals(userLogin.getUserpass())) {
                if (userLogin.getUserRole() == employeeRole) {
                    ctx.status(200);
                    log.info("employee login successful " + user);
                    return;
                } else if (userLogin.getUserRole() == customerRole)
                ctx.status(200);
                log.info("Customer login successful " + user);
                return;
            }
        }
        log.info("could not login");
        throw new UnauthorizedResponse("Wrong credentials");
    }

//    public void employeeAuthToken(Context ctx) {
//        log.info("getting employee token");
//        if (ctx.method().equals("OPTIONS")) {
//            return;
//        }
//        String authHeader = ctx.header("Authorization");
//        if (authHeader != null && authHeader.equals("employee-auth-token")) {
//            log.info("request authorized");
//        } else {
//            log.warn("improper authorization");
//            throw new UnauthorizedResponse("invalid user");
//        }
//    }
//
//    public void custAuthToken(Context ctx) {
//        log.info("getting cust token");
//        if (ctx.method().equals("OPTIONS")) {
//            return;
//        }
//        String authHeader = ctx.header("Authorization");
//        if (authHeader != null && authHeader.equals("customer-auth-token")) {
//            log.info("request authorized");
//        } else {
//            log.warn("improper authorization");
//            throw new UnauthorizedResponse("invalid user");
//        }
//    }
}