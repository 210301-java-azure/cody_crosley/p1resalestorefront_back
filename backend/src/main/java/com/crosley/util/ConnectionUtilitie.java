package com.crosley.util;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static java.lang.Class.forName;

public class ConnectionUtilitie {
    private static Connection connection;
    private static Logger log = LoggerFactory.getLogger(ConnectionUtilitie.class);
    public static Connection getConnection() throws SQLException, ClassNotFoundException {
       // Class.forName("org.postgresql.Driver");
        if (connection == null || connection.isClosed()) {
            String connectionURL = System.getenv("connectionURL");
            String username = System.getenv("username");
            String password = System.getenv("password");

            //create connection
            connection = DriverManager.getConnection(connectionURL, username, password);
        }
        log.info("connected");
        return connection;
    }

//    public static Connection getHardCodeConnection() throws SQLException, ClassNotFoundException {
//        Class.forName("org.postgresql.Driver");
//        if (connection == null || connection.isClosed()) {
//            String connectionURL = "jdbc:postgresql://localhost:5432/postgres?currentSchema= online_resale";
//            String username = "postgres";
//            String password = "myPostgreSqlAcct321!";
//
//            //create connection
//            connection = DriverManager.getConnection(connectionURL, username, password);
//        }
//        return connection;
//    }

}
