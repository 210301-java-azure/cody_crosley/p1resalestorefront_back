package com.crosley;

import com.crosley.controllers.*;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.javalin.apibuilder.ApiBuilder.*;


public class ResaleStoreApp {
    public static void main(String[] args) {
        Logger log = LoggerFactory.getLogger(ResaleStoreApp.class);
        log.info("in main");
        // should not disable all cors all the time. Should enableCorsForOrigin(ipaddress) to prevent cross site scripting
        Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins/*to enable cors when request is made, sends back headers*/ ).start(7080);
        // a initial 'options' request is sent if its okay it will send the real request. Options request ca be blocked by authorization

        ItemController itemController = new ItemController();
        UserController userController = new UserController();
        PurchasesController purchasesController = new PurchasesController();
        AuthController authController = new AuthController();

        String update = "/update";
        String remove = "/remove";

//handlers
        app.routes(() -> {
//items handlers
            //before("/items/addItem",authController::employeeAuthToken);
     //       before("/items/:id/update",authController::employeeAuthToken);
      //      before("/items/:id/remove",authController::employeeAuthToken);
            path("/items", () -> {
                get(itemController::handleGetAllItems);
                post(itemController::handlePostNewItem);
                patch(itemController::handlePostUpdateItem);
                delete(itemController::handlePostRemoveItemById);
                path("/:itemId", () -> {
                    get(itemController::handleGetItemById);

                });
            });
        });
//users handlers
        app.routes(() -> {
           // before("/users",authController::employeeAuthToken);
            path("/users", () -> {
                get(userController::handleGetAllUsers);
                post(userController::handlePostNewUser);
                patch(userController::handleUpdateUser);
                delete(userController::handleRemoveUser);
                path("/:userId", () -> {
                    get(userController::handleGetUserById);
                });
            });
            path("/:username", () -> {
                get(userController::handleGetUserByUsername);
            });

        });
//purchased handlers/update
        app.routes(() -> {
    //        before("/purchased",authController::employeeAuthToken);
            path("/purchased", () -> {
                get(purchasesController::handleGetAllPurchases);
                path("/:id", () -> {
                    get(purchasesController::handleGetPurchase);
                    path(remove, () -> {
                        post(purchasesController::handlePostRemovePurchase);
                    });
                });
                path(update, () -> {
                    post(purchasesController::handlePostNewPurchase);
                });
            });
        });
//login handlers
        app.routes(() -> {     path("/login", () -> {
                post(authController::employeeAuthLogin);
                });
            });
    }
}

