//getting item data from server


const hostPurchased = "http://13.68.227.32:80/purchased";
//const hostPurchased = "http://localhost:7080/purchased";


const xhr = new XMLHttpRequest();
xhr.open("GET", hostPurchased);//opening request to GET form server
//let token = sessionStorage.getItem("jwt");//can set the auth header with this value instead
xhr.setRequestHeader("Authorization", "employee-auth-token");//purchased table need authintication to view
xhr.onreadystatechange = function(){
    console.log("checking readystate");
    if(xhr.readyState == 4){//checking for readystate 4 for all loading to be done
        if(xhr.status == 200){
            const purchases = JSON.parse(xhr.responseText)//xhr.responseText returns in JSON needs to be parsed to a object
            //console.log(purchases);//items render in console
            renderPurchasesIntable(purchases);//items will render in a table
        }else{
            console.log("Something didnt go right")
        }
    }
}
xhr.send();

function renderPurchasesIntable(purchaseList){
    document.getElementById("purchases-table").hidden = false;
    const table = document.getElementById("purchases-table");
    for(let items of purchaseList){
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${items.itemId}</td><td>${items.quantity}</td><td>${items.soldDate}</td><td>${items.unitPrice}</td><td>${items.totalPrice}</td>`;
        table.appendChild(newRow);
    }

}