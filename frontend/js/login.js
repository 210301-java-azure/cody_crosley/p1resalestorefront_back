
document.getElementById("submit-form").addEventListener("submit", loginAttempt);

function loginAttempt(event){
    event.preventDefault();
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    console.log(username);
    ajaxLogin(username, password, successCallback, failurCallBack);
}

function successCallback(){
    console.log("attemting to retrieve token")
    getToken();
    console.log("Logged in");
    //window.location.href = "./p1ResaleStore.html";
}
function failurCallBack(){
    document.getElementById("login-fail").hidden = false;
    console.log("Get outta here");
}

