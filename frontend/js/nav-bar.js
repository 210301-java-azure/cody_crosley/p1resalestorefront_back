let custToken = sessionStorage.getItem("custAuthToken");
let empToken = sessionStorage.getItem("empAuthToken");
if(empToken){
    document.getElementById("purchases-link").hidden = false;
    document.getElementById("alter-inventory-link").hidden = false;
    document.getElementById("users-link").hidden = false;
    document.getElementById("alter-user-link").hidden = false;
    document.getElementById("login-link").hidden = true;
    document.getElementById("create-account-link").hidden = true;
    document.getElementById("sign-off-link").hidden = false;
}else if(custToken){
    document.getElementById("purchases-link").hidden = true;
    document.getElementById("alter-inventory-link").hidden = true;
    document.getElementById("users-link").hidden = true;
    document.getElementById("alter-user-link").hidden = true;
    document.getElementById("login-link").hidden = true;
    document.getElementById("create-account-link").hidden = true;
    document.getElementById("sign-off-link").hidden = false;
}else{
    document.getElementById("purchases-link").hidden = true;
    document.getElementById("alter-inventory-link").hidden = true;
    document.getElementById("users-link").hidden = true;
    document.getElementById("alter-user-link").hidden = true;
    document.getElementById("sign-off-link").hidden = true
}

document.getElementById("sign-off-link").addEventListener("click", function signOff(){
    console.log("in signOff");
    sessionStorage.clear();
    sessionStorage.removeItem("empAuthToken");
    sessionStorage.removeItem("custAuthToken");
    window.location.href= "./p1ResaleStore.html";
});

