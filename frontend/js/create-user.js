const hostUsers = "http://13.68.227.32:80/users"
//const hostUsers = "http://localhost:7080/users";
document.getElementById("create-user-form").addEventListener("submit", addNewUser );

 function addNewUser(e){
     e.preventDefault();
     
    const role = document.getElementById("user-role").value;
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    const firstname = document.getElementById("firstname").value;
    const lastname = document.getElementById("lastname").value;
    const address = document.getElementById("address").value;
    const email = document.getElementById("email").value;
    fetch(hostUsers , {
        method: 'POST',
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body:JSON.stringify({
            userRole:role,
            username:username,
            userpass:password,
            firstname:firstname,
            lastname:lastname,
            address:address, 
            email:email       
        })     
    })
    .then(console.log("successful add"))
    //window.location.replace("p1ResaleStore.html")
    .catch(error => console.log("ERROR"));
}