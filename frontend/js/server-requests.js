

const hostLogin= "http://13.68.227.32:80/login";
//const hostLogin = "http://localhost:7080/login";


function ajaxServerRequest(method, url, body, successCallback, failurCallBack, authToken){
    console.log("in server request");
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    if(authToken){//checking for authToken
        xhr.setRequestHeader("Authorization", authToken)//if found send in request
    }
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){//checking for readystate 4 for all loading to be done
            if(xhr.status > 199 && xhr.status < 300){
                
                successCallback(xhr);
            }else{
                console.log(xhr.status);
                failurCallBack(xhr);
            }
        }
    }
    if(body){
        xhr.send(body);
    }else{
        xhr.send();
    }     
}

function sendAjaxPost(url, body, successCallback, failurCallBack, authToken){
    ajaxServerRequest("POST", url, body, successCallback, failurCallBack, authToken)
}
 
function sendAjaxGet(url, body, successCallback, failurCallBack, authToken){
    ajaxServerRequest("GET", url, body, successCallback, failurCallBack, authToken)
}

function ajaxLogin(username, password, successCallback, failurCallBack){
    const payload = `username=${username}&password=${password}`;
    sendAjaxPost(hostLogin, payload, successCallback, failurCallBack);
}
