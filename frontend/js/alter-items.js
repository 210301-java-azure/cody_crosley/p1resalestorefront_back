
const hostItems = "http://13.68.227.32:80/items";
//const hostItems = "http://localhost:7080/items";

//adding item fields
document.getElementById("add-item").addEventListener("click", function(e){
    e.preventDefault;
    document.getElementById("id").hidden = true;
    document.getElementById("picture").hidden = false;
    document.getElementById("title").hidden = false;
    document.getElementById("description").hidden = false;
    document.getElementById("category").hidden = false;
    document.getElementById("condition").hidden = false;
    document.getElementById("quantity").hidden = false;
    document.getElementById("price").hidden = false;
    document.getElementById("retrieve-item-submit").hidden = true;
    document.getElementById("add-item-submit").hidden = false;
    document.getElementById("update-item-submit").hidden = true;
    document.getElementById("remove-item-submit").hidden = true;
})
//------------------------removing item fields----------------------
document.getElementById("remove-item").addEventListener("click",function(e){
    e.preventDefault();
        document.getElementById("id").hidden = false;
        document.getElementById("picture").hidden = true;
        document.getElementById("title").hidden = true;
        document.getElementById("description").hidden = true;
        document.getElementById("category").hidden = true;
        document.getElementById("condition").hidden = true;
        document.getElementById("quantity").hidden = true;
        document.getElementById("price").hidden = true;
        document.getElementById("retrieve-item-submit").hidden = true;
        document.getElementById("add-item-submit").hidden = true;
        document.getElementById("update-item-submit").hidden = true;
        document.getElementById("remove-item-submit").hidden = false;
})
//---------------updateing item fields-------------------------------
document.getElementById("update-item").addEventListener("click",function(e){
    e.preventDefault();
    document.getElementById("id").hidden = false;
    document.getElementById("picture").hidden = true;
    document.getElementById("title").hidden = true;
    document.getElementById("description").hidden = true;
    document.getElementById("category").hidden = true;
    document.getElementById("condition").hidden = true;
    document.getElementById("quantity").hidden = true;
    document.getElementById("price").hidden = true;
    document.getElementById("retrieve-item-submit").hidden = false;
    document.getElementById("update-item-submit").hidden = true;
    document.getElementById("add-item-submit").hidden = true;
    document.getElementById("remove-item-submit").hidden = true;
})

  
//--------------------------add item------------------------------------
document.getElementById("add-item-submit").onclick = addNewItem ;

function addNewItem(e){
    e.preventDefault();
const name = document.getElementById("item-title").value;
const itemPicture = document.getElementById("item-picture").value;
const description = document.getElementById("item-description").value;
const category = document.getElementById("item-category").value;
const condition = document.getElementById("item-condition").value;
const quantity = document.getElementById("item-quantity").value;
const price = document.getElementById("item-price").value;
console.log(itemPicture);
fetch(hostItems , {
    method: 'POST',
    headers: {
        "Content-type": "application/json; charset=UTF-8"
    },
    body:JSON.stringify({
        itemName:name,
        picture:itemPicture,
        itemDescription:description,
        itemCategory:category,
        itemCondition:condition,
        itemQuantity:quantity, 
        itemPrice:price       
    })     
})
.then(
    document.getElementById("alter-item-msg").hidden=false,
    console.log("add successful"))
.catch(error => console.log("ERROR"));
}


//----------------------remove item--------------------------------
document.getElementById("remove-item-submit").onclick = removeItem;

function removeItem(e){
    e.preventDefault();  
const id = document.getElementById("item-id").value;

fetch( hostItems, {
    method:"DELETE",
    headers: {
        "Content-type": "application/json; charset=UTF-8"
    },    
    body: id
})     
.then(
    document.getElementById("alter-item-msg").hidden=false,

    document.getElementById("alter-item-msg").nodeValue = "Item removed",
    console.log("item removed"))
.catch(error => console.log("ERROR"));
}


//-------------------------update item------------------------------------
//------------------retrieving item to update----------------------------
document.getElementById("retrieve-item-submit").onclick = retrieveItem;
function retrieveItem(e){
    e.preventDefault(); 
    const itemToUpdate = document.getElementById("item-id").value;
console.log(itemToUpdate);
    sendAjaxGet(hostItems+"/"+itemToUpdate, undefined, successCallback1, failurCallBack1, undefined);
    console.log(hostItems+"/"+itemToUpdate);
    function successCallback1(xhr){
        console.log("item retrieved");
        var item = JSON.parse(xhr.responseText);
        console.log(item);
        document.getElementById("id").hidden = false;
        document.getElementById("picture").hidden = false;
        document.getElementById("title").hidden = false;
        document.getElementById("description").hidden = false;
        document.getElementById("category").hidden = false;
        document.getElementById("condition").hidden = false;
        document.getElementById("quantity").hidden = false;
        document.getElementById("price").hidden = false;
        document.getElementById("retrieve-item-submit").hidden = true;
        document.getElementById("update-item-submit").hidden = false;

        document.getElementById("item-picture").value= item.picture;
        document.getElementById("item-title").value= item.itemName;
        document.getElementById("item-description").value= item.itemDescription;
        document.getElementById("item-category").value= item.itemCategory;
        document.getElementById("item-condition").value= item.itemCondition;
        document.getElementById("item-quantity").value= item.itemQuantity;
        document.getElementById("item-price").value= item.itemPrice;
        //console.log(thisThing);
    }
    function failurCallBack1(){
        console.log("item not retrieved");
    }
}
//------------------------updating-------------------------------------
document.getElementById("update-item-submit").onclick = updateItem;
function updateItem(e){
    e.preventDefault();
document.getElementById("item-id").hidden = false;
document.getElementById("item-id").disabled = true;
const id = document.getElementById("item-id").value;  
const name = document.getElementById("item-title").value;
const itemPicture = document.getElementById("item-picture").value;
const description = document.getElementById("item-description").value;
const category = document.getElementById("item-category").value;
const condition = document.getElementById("item-condition").value;
const quantity = document.getElementById("item-quantity").value;
const price = document.getElementById("item-price").value;
console.log(itemPicture);
    fetch( hostItems, {
        method:"PUT",
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },    
        body:JSON.stringify({
            itemId:id,
            picture:itemPicture,
            itemName:name,  
            itemCategory:category,
            itemCondition:condition,
            itemDescription:description,
            itemQuantity:quantity, 
            itemPrice:price 
        })  
    })   
    .then(console.log("item updated"),
    document.getElementById("alter-item-msg").hidden=false,
    document.getElementById("staticBackdropLabel").value = "Item Updated")
    .catch(error => console.log("ERROR"))   
}
