var itemsList = [];
var inventory = [];
let items;
const hostItems = "http://13.68.227.32:80/items";
//const hostItems = "http://localhost:7080/items";
sendAjaxGet(hostItems, undefined, successfullCallBack, failurCallBack, undefined)


function successfullCallBack(xhr){
    itemsList = JSON.parse(xhr.responseText);
    console.log(itemsList);
    renderItemsInTable(itemsList);
}
function failurCallBack(xhr){
console.log("something didnt go right");
}


function renderItemsInTable(itemsList){
    const table = document.getElementById("items-table-body");
    //console.log(itemsList);
    for( items of itemsList){
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${items.itemId}</td><td><img style="hight:5vw; width:5vw" src=${items.picture}></td><td>${items.itemName}</td><td><textarea readonly id="item-description" class="form-control" aria-label="With textarea">${items.itemDescription}</textarea></td><td>${items.itemCategory}</td><td>${items.itemCondition}</td><td>${items.itemQuantity}</td><td>${items.itemPrice}</td>
        <td>
            <input type="checkbox" class="btn-check" name="btn" id="btn${items.itemId}" autocomplete="off" >
            <label class="btn btn-outline-primary" for="btn${items.itemId}">Add to cart</label>
        </td>`;
        table.appendChild(newRow);           
    }
}

//--------------------Getting checked items-------------------------------------------------
document.getElementById("chkout-btn").addEventListener("click", checkout); 
function checkout(){
    for(items of itemsList){
        if(document.getElementById("btn"+ items.itemId).checked){
            inventory.push(items);
        }  
    }
    console.log(inventory);
}